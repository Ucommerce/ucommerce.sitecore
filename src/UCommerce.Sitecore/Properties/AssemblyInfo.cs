using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("UCommerce.Sitecore")]
[assembly: AssemblyDescription("Ucommerce integration with Sitecore")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Ucommerce")]
[assembly: AssemblyProduct("UCommerce.Sitecore")]
[assembly: AssemblyCopyright("Copyright � 2014")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("8FA07A91-1CB2-4DAD-9A94-580C070C56A5")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("8.3.0.19193")]
[assembly: AssemblyVersion("8.3.0.19193")]
[assembly: AssemblyFileVersion("8.3.0.19193")]
[assembly: AssemblyInformationalVersion("8.3.0.19193 47b672cc75b1+ (Release-Candidate-8-3-0) tip")]
